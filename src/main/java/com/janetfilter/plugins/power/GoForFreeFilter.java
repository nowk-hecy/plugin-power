package com.janetfilter.plugins.power;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.janetfilter.core.commons.DebugInfo;
import com.janetfilter.core.enums.RuleType;
import com.janetfilter.core.models.FilterRule;

/**
 * GoForFreeFilter
 *
 * @Date: 2024-04-25 9:11
 * @Version: 1.0
 */
public class GoForFreeFilter {
    private static Map<String, String> freeMap;

    public static void setRules(List<FilterRule> rules) {
        freeMap = new HashMap<>();
        for (FilterRule rule : rules) {
            if (rule.getType() != RuleType.EQUAL) {
                continue;
            }
            String[] split = rule.getRule().split("->", 2);
            freeMap.put(split[0], split[1]);
        }
    }

    public static String testFilter(String encode) {
        DebugInfo.debug("GoForFreeFilter>>>>testFilter:" + encode);
        if (freeMap.containsKey(encode)) {
            return freeMap.get(encode);
        }
        return encode;
    }
}
