package com.janetfilter.plugins.power;


import static jdk.internal.org.objectweb.asm.Opcodes.ALOAD;
import static jdk.internal.org.objectweb.asm.Opcodes.ASM5;
import static jdk.internal.org.objectweb.asm.Opcodes.ASTORE;
import static jdk.internal.org.objectweb.asm.Opcodes.INVOKESTATIC;

import java.util.List;

import com.janetfilter.core.models.FilterRule;
import com.janetfilter.core.plugin.MyTransformer;

import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.ClassWriter;
import jdk.internal.org.objectweb.asm.tree.ClassNode;
import jdk.internal.org.objectweb.asm.tree.InsnList;
import jdk.internal.org.objectweb.asm.tree.MethodInsnNode;
import jdk.internal.org.objectweb.asm.tree.MethodNode;
import jdk.internal.org.objectweb.asm.tree.VarInsnNode;

/**
 * GoForFreeTransformer
 *
 * @Date: 2024-04-25 9:12
 * @Version: 1.0
 */
public class GoForFreeTransformer  implements MyTransformer {
    public GoForFreeTransformer(List<FilterRule> rules)
    {
        GoForFreeFilter.setRules(rules);
    }

    public String getHookClassName()
    {
        return "java/text/SimpleDateFormat";
    }

    public byte[] transform(String className, byte[] classBytes, int order)
            throws Exception
    {
        System.out.println("your yd=lsl, ----------------");
        ClassReader reader = new ClassReader(classBytes);
        ClassNode node = new ClassNode(ASM5);
        reader.accept(node, 0);

        for (MethodNode mn : node.methods)
        {
            if ("parse".equals(mn.name)) {
                System.out.println(mn.desc);

                InsnList list = new InsnList();

                list.add(new VarInsnNode(ALOAD, 1));
                list.add(new MethodInsnNode(INVOKESTATIC, "com/janetfilter/plugins/power/GoForFreeFilter", "testFilter", "(Ljava/lang/String;)Ljava/lang/String;", false));
                list.add(new VarInsnNode(ASTORE, 1));
                mn.instructions.insert(list);
            }
        }

        ClassWriter writer = new ClassWriter(3);
        node.accept(writer);

        return writer.toByteArray();
    }
}
